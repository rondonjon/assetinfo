import { getIsinForWkn } from "../datasources/getIsinForWkn/getIsinForWkn"
// import { resolveDetail } from "./resolveDetail"

export const resolveWknToIsin = async (_parent: unknown, { wkn }: { wkn: string }, _context: unknown) => {
  const { isin } = await getIsinForWkn(wkn)
  return {
    wkn,
    isin,
    // detail: resolveDetail,
  }
}
