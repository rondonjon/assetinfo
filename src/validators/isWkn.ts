/**
 * Checks if the given `string` is a valid WKN.
 *
 * The requirements for valid WKNs hav been relaxed several times, with the latest definition
 * being that [any composition of six digits and/or capital letters qualifies as WKN](https://en.wikipedia.org/wiki/Wertpapierkennnummer).
 */
export const isWkn = (s: string) => s.match(/^[A-Z0-9]{6}$/) !== null
