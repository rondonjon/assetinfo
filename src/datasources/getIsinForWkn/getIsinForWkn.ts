import fetch from "node-fetch"
import { isIsin } from "../../validators/isIsin"
import { isWkn } from "../../validators/isWkn"

type ApiResponse = {
  uuid: string
  name: string | null
  isin: string | null
  wkn: string
  symbolXfra: string | null
  symbolXnas: string | null
  symbolXnys: string | null
  securityType: string
  markets: {
    marketCode: string
    symbol: string
    firstPriceDate: string | null
    lastPriceDate: string | null
    currencyCode: string | null
  }[]
}[]

export type WknIsin = {
  wkn: string
  isin: string
}

/**
 * Retrieves the {@linkcode ISIN} for a given {@linkcode WKN} from [portfolio-report.net](https://www.portfolio-report.net).
 *
 * Rejects the promise if an error occurs.
 *
 * @param wkn
 */
export const getIsinForWkn = async (wkn: string): Promise<WknIsin> => {
  if (!isWkn(wkn)) {
    throw new Error("Invalid WKN")
  }

  let body: ApiResponse

  try {
    const response = await fetch(`https://api.portfolio-report.net/securities/search/${wkn}`)

    if (response.status !== 200) {
      throw new Error(`Unable to resolve WKN "${wkn}": service request failed with status code ${response.status}`)
    }

    body = (await response.json()) as ApiResponse
  } catch (err) {
    throw new Error(`Unable to resolve WKN "${wkn}": service request failed: ${(err as Error).message}`)
  }

  if (!Array.isArray(body)) {
    throw new Error(`Unable to resolve WKN "${wkn}": unexpected array format`)
  }

  if (body.length === 0) {
    throw new Error(`Unable to resolve WKN "${wkn}": empty response`)
  }

  if (typeof body[0] !== "object" || typeof body[0].wkn !== "string" || typeof body[0].isin !== "string") {
    throw new Error(`Unable to resolve WKN "${wkn}": unexpected object format`)
  }

  if (body[0].wkn !== wkn) {
    throw new Error(`Unable to resolve WKN "${wkn}": ID mismatch in response`)
  }

  const isin = body[0]?.isin

  if (!isIsin(isin)) {
    throw new Error(`Unable to resolve WKN "${wkn}": invalid ISIN in response`)
  }

  return {
    wkn,
    isin,
  }
}
