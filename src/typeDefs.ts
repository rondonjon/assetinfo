import { gql } from "apollo-server"

export const typeDefs = gql`
  type Detail {
    value: Int!
  }

  type WknIsin {
    wkn: String!
    isin: String!
    # detail: Detail!
  }

  type Query {
    wknToIsin(wkn: String!): WknIsin
  }
`
