import { ApolloServer } from "apollo-server"
import { ApolloServerPluginLandingPageLocalDefault } from "apollo-server-core"
import { resolveWknToIsin } from "./resolvers/resolveWknToIsin"
import { typeDefs } from "./typeDefs"

const server = new ApolloServer({
  plugins: [
    ApolloServerPluginLandingPageLocalDefault, // always enable the Sandbox, even on PROD, see https://www.apollographql.com/docs/apollo-server/api/plugin/landing-pages/
  ],
  typeDefs,
  resolvers: {
    Query: {
      wknToIsin: resolveWknToIsin,
    },
  },
  persistedQueries: false, // true is a security/DDoS risk in Production
})

// The `listen` method launches a web server.
server
  .listen({
    port: process.env.PORT || 8000,
  })
  .then(({ url }) => {
    console.log(`🚀 AssetInfo Apollo Server ready at ${url}`)
  })
